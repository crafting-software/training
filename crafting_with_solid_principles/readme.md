
# Crafting with SOLID principles

Sample in java.


## References

- https://en.wikipedia.org/wiki/SOLID
- http://www.arolla.fr/blog/2017/02/principes-solid-vie-de-jours




# Détails

- Faire une liste plus précises des bad smells avec des exemples
- Donner des exemples d’implémentation


## Single responsibility

- Quelle granularité ?
- Fusion de sémantiques dans une variable / objet
- Vérifier l’opportunité des 'else'


## Open/Close

- Perte de lisibilité / clarté
- Nécessité d'un protocole de configuration
- DSL
- Quand est-ce qu'il faut rendre un code open/close ? (quand est-ce que cette règle à changé la dernière fois ?)

Différente méthodes pour rendre open/close :
- Chaîne de délégation
- Monoïdes
- Stratégies
- Map


## Liskov

Différents cas de violation.


## Inteface segregation

- Designer une interface ne fonction de son utilisation et pas en fonction de son provider.
- Plus les interfaces son petites, plus elles sont réutilisables
- Utiliser des adapters
- Une grosse interface risque d'aboutir sur une violation de Liskov


## Inversion dépendance

- Banana monkey jungle problem
- Difficile à tester


## Demeter

- Encapsulation
